/**
 * Import the polyfills and vendor files
 */
import './polyfills';
import './vendor';


import * as angular from 'angular';
import { IStateProvider, IUrlRouterProvider } from 'angular-ui-router';

import AppComponent from './app.component';
import Components from './components';

/**
 * Import the global styles
 */
import '../static/style/includes.scss';

const app = angular
    .module('app', [
        Components
    ])
    .component('app', AppComponent)
    .config(function ($stateProvider: IStateProvider, $urlRouterProvider: IUrlRouterProvider) {
        $urlRouterProvider.otherwise('/login');
    })
    .name;

export default app;
