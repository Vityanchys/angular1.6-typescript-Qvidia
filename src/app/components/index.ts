import * as angular from 'angular';
import HelloWorld from './hello-world';
import Auth from './auth';
import Home from './home';

const components = angular
    .module('app.components', [
        HelloWorld,
        Auth,
        Home
    ])
    .name;

export default components;
