import * as angular from 'angular';
import 'angular-mocks';
import { ICompileService, IRootScopeService }  from 'angular';
import { HelloWorldService as Service } from './hello-world.service';
import { HelloWorldController as Controller } from './hello-world.controller';

describe('Hello world service', () => {
    let service: Service;
    beforeEach(angular.mock.module('helloWorld'));
    beforeEach(angular.mock.inject((_HelloWorldService_: Service) => {
        service = _HelloWorldService_;
    }));
    it('return hello world string', () => {
        expect(service.getHello()).toBe('Hello World!');
    });
});


describe('Hello world component', () => {
    let $compile: ICompileService;
    let $rootScope: IRootScopeService;
    let compiledComponent: any;
    beforeEach(angular.mock.module('helloWorld'));
    beforeEach(angular.mock.inject((_$compile_: ICompileService, _$rootScope_: IRootScopeService) => {
        $rootScope = _$rootScope_.$new();
        $compile = _$compile_;
        let element = angular.element('<hello-world></hello-world>');
        compiledComponent = $compile(element)($rootScope);
        $rootScope.$digest();
    }));

    describe('When hello world component is compiled ', () => {
        it('it is not null and contains div.greeting', () => {
            const  componentElement = compiledComponent[0];
            expect(componentElement).toBeDefined();
            expect(componentElement.querySelector('.greeting')).not.toEqual(null);
        });
    });
});
