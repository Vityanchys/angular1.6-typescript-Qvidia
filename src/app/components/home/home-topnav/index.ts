import * as angular from 'angular';

import homeTopNavComponent from './home-topnav.component';
import {IStateProvider, IUrlRouterProvider } from 'angular-ui-router';

const homeTopNav = angular
    .module('homeTopNav', ['ui.router'])
    .component('homeTopNav', homeTopNavComponent)
    .config(function ($stateProvider: IStateProvider, $urlRouterProvider: IUrlRouterProvider) {
        $stateProvider
            .state('home.topnav', {
                url: '/home',
                component: 'homeTopNav'
            });
    })
    .name;


export default homeTopNav;
