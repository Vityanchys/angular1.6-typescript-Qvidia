import { HomeTopNavController as controller } from './home-topnav.controller';

const homeTopNavComponent = {
    template: require('./home-topnav.html')
};

export default homeTopNavComponent;
