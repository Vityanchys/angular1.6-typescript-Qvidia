import * as angular from 'angular';
import UiRouter from '@uirouter/angularjs';
import HomeComponent from './home.component';
import { IStateProvider, IUrlRouterProvider } from 'angular-ui-router';
import HomeTopNav from './home-topnav';


const home = angular
    .module('home', [
        UiRouter,
        HomeTopNav
    ])
    .component('homeComponent', HomeComponent)
    .config(function ($stateProvider: IStateProvider, $urlRouterProvider: IUrlRouterProvider) {
        $stateProvider
            .state('home', {
                component: 'homeComponent'
            });
    })
    .name;

export default home;
