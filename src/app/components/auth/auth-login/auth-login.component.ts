import { AuthLoginController as controller } from './auth-login.controller';

const authLoginComponent = {
    template: require('./auth-login.html')
};

export default authLoginComponent;
