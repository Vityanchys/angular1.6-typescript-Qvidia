import * as angular from 'angular';

import AuthLoginComponent from './auth-login.component';
import {IStateProvider, IUrlRouterProvider } from 'angular-ui-router';

const authLogin = angular
    .module('authLogin', ['ui.router'])
    .component('authLogin', AuthLoginComponent)
    .config(function ($stateProvider: IStateProvider, $urlRouterProvider: IUrlRouterProvider) {
        $stateProvider
            .state('auth.login', {
                url: '/login',
                component: 'authLogin'
            });
    })
    .name;


export default authLogin;
