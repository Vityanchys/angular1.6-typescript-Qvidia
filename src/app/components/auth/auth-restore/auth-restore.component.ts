import { AuthRestoreController as controller } from './auth-restore.controller';

const authRestoreComponent = {
    template: require('./auth-restore.html')
};

export default authRestoreComponent;
