import * as angular from 'angular';

import AuthRestoreComponent from './auth-restore.component';
import { IStateProvider, IUrlRouterProvider } from 'angular-ui-router';

const authRestore = angular
    .module('authRestore', [])
    .component('authRestore', AuthRestoreComponent)
    .config(function ($stateProvider: IStateProvider, $urlRouterProvider: IUrlRouterProvider) {
        $stateProvider
            .state('auth.restore', {
                url: '/restore',
                component: 'authRestore'
            });
    })
    .name;


export default authRestore;
