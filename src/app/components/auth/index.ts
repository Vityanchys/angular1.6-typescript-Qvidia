import * as angular from 'angular';
import AuthLogin from './auth-login';
import AuthRestore from './auth-restore';
import AuthComponent from './auth.component';

import UiRouter from '@uirouter/angularjs';
import { IStateProvider, IUrlRouterProvider } from 'angular-ui-router';

const auth = angular
    .module('auth', [
        UiRouter,
        AuthLogin,
        AuthRestore
    ])
    .component('authComponent', AuthComponent)
    .config(function ($stateProvider: IStateProvider, $urlRouterProvider: IUrlRouterProvider) {
        $stateProvider
            .state('auth', {
                component: 'authComponent'
            });
    })
    .name;

export default auth;
